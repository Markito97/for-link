require("dotenv").config()
const express = require("express");
const userRouter= require("./routes/user.routes")
const PORT = process.env.PORT || 5000;
const cors = require("cors")
const app = express();

app.use(cors())
app.use(express.json())
app.use("/api",userRouter);


const start = async () => {
    try {
        app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
    } catch(err) {
        console.log(err)
    }
}

start()